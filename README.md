# Wavecut audio file analyzer

## Installation

- Clone this repository
    
    
        git clone https://gitlab.com/rpiguru/Wavecut

- Install dependencies
    
        
        cd Wavecut
        pip3 install -r requirements.txt

## Usage
    
    
    python3 wc_process.py --file [file name]
