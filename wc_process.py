"""
    Wavecut audio file analyzer

"""

import argparse
import os
import time
import librosa
import audioread
import fleep


def _get_file_format(file_path):
    with open(file_path, "rb") as file:
        info = fleep.get(file.read(128))
    return dict(type=info.type[0], extension=info.extension[0], mime=info.mime[0])


def analyze_audio_file(file_path):
    if not os.path.exists(file_path):
        raise FileNotFoundError(file_path)

    s_time = time.time()
    result = {}

    with audioread.audio_open(file_path) as f:
        result['channels'] = f.channels
        result['sample_rate'] = f.samplerate
        result['duration'] = f.duration
    file_info = _get_file_format(file_path)
    result['format'] = file_info['extension']

    # Calculate BPM and Beats
    y, sr = librosa.load(file_path, sr=None)
    tempo, beats = librosa.beat.beat_track(y=y, sr=sr)
    result['bpm'] = tempo
    result['beats'] = beats

    # Calculate onset times
    onset_frames = librosa.onset.onset_detect(y=y, sr=sr)
    onset_times = librosa.frames_to_time(onset_frames, sr=sr)
    result['onset_times'] = onset_times

    tuning = librosa.estimate_tuning(y=y, sr=sr)
    result['turning'] = tuning

    result['_elapsed_'] = time.time() - s_time
    return result


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', nargs='?', help='File Name', required=True)

    args = parser.parse_args()

    _info = analyze_audio_file(args.file)

    import pprint
    pprint.pprint(_info)
